import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

  /**
  * Ajoute deux entiers
  * @param a le premier entier à ajouter
  * @param b le deuxieme entier à ajouter
  * @return la somme des deux entiers
  */
  public static Integer plus(Integer a, Integer b){
    return a+b;
  }


  /**
  * Renvoie la valeur du plus petit élément d'une liste d'entiers
  * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
  * @param liste
  * @return le plus petit élément de liste
  */
  public static Integer min(List<Integer> liste){

    if(liste.isEmpty()){
      return null;
    }
    int min=liste.get(0);
    for (int i=1;i<liste.size();i++){
      if(liste.get(i)<min){
        min=liste.get(i);
      }
    }
    return min;
  }


  /**
  * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
  * @param valeur
  * @param liste
  * @return true si tous les elements de liste sont plus grands que valeur.
  */
  public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
    for(T elem:liste){
      if(elem.compareTo(valeur)<1){
        return false;
      }
    }
    return true;
  }



  /**
  * Intersection de deux listes données par ordre croissant.
  * @param liste1 une liste triée
  * @param liste2 une liste triée
  * @return une liste triée avec les éléments communs à liste1 et liste2
  */
  public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
    List<T> liste = new ArrayList<>();
    if(liste1.equals(liste2) && !liste1.isEmpty()){
      T precedent = liste1.get(0);
      liste.add(precedent);
      for(T elem : liste1){
        if (elem!= precedent){
          liste.add(elem);
        }
        precedent = elem;
      }
      return liste;
    }

    int cptlist1 = 0;
    int cptlist2 = 0;

    while(cptlist1 <= liste1.size() - 1 && cptlist2 <= liste2.size() - 1){
      if((liste1.get(cptlist1).compareTo(liste2.get(cptlist2)))==0){
        if(!liste.contains(liste1.get(cptlist1))){
          liste.add(liste1.get(cptlist1));
        }
        cptlist1++;
        cptlist2++;
      }
      else if((liste1.get(cptlist1).compareTo(liste2.get(cptlist2)))==1){
        cptlist2++;
      }
      else{
        cptlist1++;
      }
    }

    return liste;

  }



  /**
  * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
  * @param texte une chaine de caractères
  * @return une liste de mots, correspondant aux mots de texte.
  */
  public static List<String> decoupe(String texte){
    String mot="";
    List<String> listemots=new ArrayList<>();

    for(char carac: texte.toCharArray()){
      if(carac== ' ' && !mot.equals("")){
        listemots.add(mot);
        mot="";
      }
      else if(carac !=' '){
        mot+=carac;
      }
    }
    if(!mot.equals("")){
      listemots.add(mot);
    }
    return listemots;
  }


  /**
  * Renvoie le mot le plus présent dans un texte.
  * @param texte une chaine de caractères
  * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
  */

  public static String motMajoritaire(String texte){
    if(texte.equals("")){
      return null;
    }

    HashMap<String,Integer> listemot=new HashMap<>();
    List<String> textecomplet=decoupe(texte);

    for(String mot:textecomplet){
      if(listemot.containsKey(mot)){
        listemot.put(mot,listemot.get(mot)+1);
      }
      else{
        listemot.put(mot,1);
      }
    }

    String pluspresent="";
    listemot.put("",1);

    for(String mot:listemot.keySet()) {
      if (listemot.get(mot).equals(listemot.get(pluspresent))) {
        List<String> liste = new ArrayList<>();
        liste.add(mot);
        liste.add(pluspresent);
        pluspresent = Collections.min(liste);
      }
      if (listemot.get(mot) > listemot.get(pluspresent)) {
        pluspresent = mot;
      }
    }

    return pluspresent;

  }

  /**
  * Permet de tester si une chaine est bien parenthesée
  * @param chaine une chaine de caractères composée de ( et de )

  * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
  */
  public static boolean bienParenthesee(String chaine) {
    if (chaine.isEmpty()) {
      return true;
    }
    int nbrparenthese = 0;
    for (char carac : chaine.toCharArray()) {
      if (carac != '(') {
        nbrparenthese -= 1;
      }
      if (carac != ')') {
        nbrparenthese += 1;
      }
      if (nbrparenthese < 0) {
        return false;
      }
    }
    return nbrparenthese==0;
  }

  /**
  * Permet de tester si une chaine est bien parenthesée
  * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
  * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
  */
  public static boolean bienParentheseeCrochets(String chaine){
    List<Character> liste = new ArrayList<>();
    char carac;
    for(int i = 0; i < chaine.length(); i++)
    {
      carac = chaine.charAt(i);
      if(carac == '(' || carac == '[')
      liste.add(carac);

      else if(carac == ']'){
        if(liste.size() == 0 || !liste.get(liste.size() - 1).equals('['))
        return false;
        else
        liste.remove(liste.size() - 1);
      }

      else if(carac == ')'){
        if(liste.size() == 0 || !liste.get(liste.size() - 1).equals('('))
        return false;
        else
        liste.remove(liste.size() - 1);
      }
    }

    return liste.size() == 0;

  }


  /**
  * Recherche par dichtomie d'un élément dans une liste triée
  * @param liste, une liste triée d'entiers
  * @param valeur un entier
  * @return true si l'entier appartient à la liste.
  */
  public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
    if(liste.isEmpty()){
      return false;
    }
    if (liste.size()>0){
      int milieu;
      int debut=0;
      int fin=liste.size();

      while (debut<fin){
        milieu=((debut+fin)/2);
        if(valeur.compareTo(liste.get(milieu))==0){
          return true;
        }
        else if(valeur.compareTo(liste.get(milieu))>0){
          debut= milieu+1;
        }
        else{
          fin=milieu;
        }
      }
    }
    return false;
  }



}
